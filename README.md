> # 2022 空间计量专题 · [连享会](https://www.lianxh.cn)

&emsp;

> &#x2728; [连享会·听课建议](https://www.bilibili.com/video/BV1jv411q77T) ( [Bilibili](https://space.bilibili.com/546535876) )

> &#x1F449;  常见问题解答和每日 FAQs 在这里 【[Wiki](https://gitee.com/arlionn/SP/wikis)】     
     
> &#x1F34F; 点击右上角【[Fork](https://gitee.com/arlionn/SP#)】按钮，把本仓库复制到你的码云页面下。

&emsp; 

## 1. 课程概览

- **时间：** 2022 年 9 月 17-18 日 ；24-25 日
- **地点：** 网络直播
- **主讲嘉宾**：杨海生 (中山大学)；范巧 (兰州大学)
- **授课方式：**
  - 每天 6 小时 (9:00-12:00；14:30-17:30) + 半小时答疑
  - 讲义电子版于开课前一周发送, 内含软件安装指南
  - 软件：Stata + Matlab (R2019a, R2020a)
- **报名连接：** <http://junquan18903405450.mikecrm.com/EmGijiK>
- **课程主页：** <https://gitee.com/lianxh/SP>；[<font color=red>PDF 课纲</font>](https://file.lianxh.cn/KC/lianxh_SP.pdf)
- **参考文献：** [点击查看所有文献](https://www.jianguoyun.com/p/DfRQsIkQtKiFCBiYm_cD)
- **助教招聘**：https://www.wjx.top/jq/90823024.aspx
- **预习-回放及课件**：
  - [公开课 - 杨海生 - 如何玩转空间计量？](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E6%9D%A8%E6%B5%B7%E7%94%9F-%E7%8E%A9%E8%BD%AC%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F.md)
  - [公开课 - 范巧 - 空间计量经济学基本框架和分析范式](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)

<div STYLE="page-break-after: always;"></div>

> ${\color{red}{\text{What is NEW ?}}}$

本次课程为期 4 天，分为 A 和 B 两个部分。两位老师全面更新了课件，增加了近两年的一些新的研究成果的解读和分享。相对于 2021 年的课程，做了如下几方面的 **优化和更新**：

- **课件：** 讲义进行了全面更新 (约 70%)，更强调对模型含义和结果的解读，以便与最新的实证应用相结合。

- **代码：** 结构进行了优化，以便适度分离论文复现和原理解读两类代码。前者易于与最新的期刊论文结果对照，也便于迁移到自己的论文中；后这有助于深入理解模型背后的原理，提高自己的分析和变通能力。

- **模型和方法：** 新增了基于「投入-产出表」和「社会网络」的权重设定方法；对空间权重的的设计方式进行了更为系统的介绍；新增了动态面板空间计量模型的非平稳性及其误差修正模型；新增了面板数据的时空地理加权回归模型的最近进展和应用。

**各个专题的更新情况说明如下：**

- 第 1 讲，新增了基于「投入-产出表」和「社会网络」的权重设定方法 (Pesaran and Yang, [2020](https://file.lianxh.cn/Refs/SP/Yang/Pesaran-2020-Econometric-Analysis-of-Production-Networks.pdf), JoE; Aquaro, Bailey and Pesaran, [2021](https://www.jianguoyun.com/p/DS-tCgoQtKiFCBiZk_cD); Keane and Neal, [2020](https://www.jianguoyun.com/p/DTBst8oQtKiFCBiuk_cD) )，并通过多篇 Top 期刊论文讲解其应用要点。

- 第 3 讲，新增了高级空间滞后模型在公司金融领域的应用案例：杨海生等 ([2020](https://file.lianxh.cn/Refs/SP/Yang/%E6%9D%A8%E6%B5%B7%E7%94%9F-2020-%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%AD%A3%E5%88%8A.pdf), 《经济学 (季刊)》)，该案例为应对「同行效应」研究中普遍面临的内生性问题，提供了一种基于空间计量方法构造工具变量的全新思路。该方法可以应用于各类针对微观数据的同行效应分析。

- 第 3 讲，空间联立方程模型部分，新增了网络分析中对「主导单元」的识别和分析方法 (Pesaran and Yang, [2020](https://www.jianguoyun.com/p/DWDPCvwQtKiFCBi_nPcD))，其政策含义甚为丰富，可以应用于国际贸易、企业投融资，以及区域经济等方向。、

- 第 4 讲，「同群效应」部分，新增了如何分析金融网络的中心度和稳定性 (Li and Schürhoff, [2019](https://www.jianguoyun.com/p/DXzOQLYQtKiFCBjg0s8EIAA), JF)；增加了杨海生老师团队新近发表的论文的讲解，涉及中国房价的溢出效应等话题 (Lu Li and Yang, [2021](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/Yang/Lu_Li_Yang-2021_JAE.pdf), JAE；Lu Li and Yang, 2022, EM, R&R)。

- 第 5 讲，新增了：a. 空间权重矩阵的组合与分解；b. 内生时空权重矩阵的设计与遴选；c. 动态面板空间计量模型的非平稳性及其误差修正模型。

- 第 7 讲，新增了：a. 多尺度地理加权回归模型 (MGWR)；b. 地理加权回归模型方法的新进展和应用 (范巧和郭爱君, [2021a](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B%E6%96%B9%E6%B3%95%E4%B8%8E%E7%A0%94%E7%A9%B6%E6%96%B0%E8%BF%9B%E5%B1%95.pdf), [2021b](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%9F%BA%E4%BA%8E%E5%85%A8%E6%81%AF%E6%98%A0%E5%B0%84%E7%9A%84%E9%9D%A2%E6%9D%BF%E6%97%B6%E7%A9%BA%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B.pdf))

- 第 8 讲，新增了 PGTWR 模型的两篇应用范例：范巧和郭爱君, [2021](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%9F%BA%E4%BA%8E%E5%85%A8%E6%81%AF%E6%98%A0%E5%B0%84%E7%9A%84%E9%9D%A2%E6%9D%BF%E6%97%B6%E7%A9%BA%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B.pdf)；郭爱君和范巧, [2022](https://file.lianxh.cn/Refs/SP/FanQ/%E9%83%AD%E7%88%B1%E5%90%9B-%E8%8C%83%E5%B7%A7-2022-%E4%B8%AD%E5%9B%BD%E5%9C%B0%E7%BA%A7%E5%B8%82%E5%B7%A5%E4%B8%9A%E5%85%A8%E8%A6%81%E7%B4%A0%E7%94%9F%E4%BA%A7%E7%8E%87%E7%9A%84%E5%B1%80%E9%83%A8%E6%B5%8B%E5%BA%A6%E7%A0%94%E7%A9%B6.pdf)。

&emsp;

<div STYLE="page-break-after: always;"></div>

## 2. 主讲嘉宾简介

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E6%9D%A8%E6%B5%B7%E7%94%9F-%E5%B7%A5%E4%BD%9C%E7%85%A7-2022-120.png)

[杨海生](https://lingnan.sysu.edu.cn/faculty/yanghaisheng), 中山大学岭南学院经济学系副教授, 主要研究领域为空间计量经济学理论与应用、实证金融。在 ACM Computing Surveys、 Emerging Markets Review、 Economic Geography、Ecological Economics、《经济研究》、《管理世界》、《经济学（季刊）》、《管理科学学报》、《金融研究》、《会计研究》、《世界经济》、《系统工程理论与实践》等学术刊物上发表数十篇论文, 主持和参与多项国家自然科学基金、广东省自然科学基金等课题研究。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E8%8C%83%E5%B7%A7%E5%B7%A5%E4%BD%9C%E7%85%A7110.png)

**范巧**, 兰州大学经济学院博士研究生, 教授、硕士生导师, 研究方向为区域与城市经济发展战略, 曾任 Texas Tech University 访问学者、中国人民大学高级访问学者、重庆市社科联理论研究室副主任；全国投入产出与大数据研究会常务理事、重庆市数量经济学会理事、重庆市商务委评标专家；在经济科学出版社等出版专著 3 部, 在《数量经济技术经济研究》、Applied Mathematics and Nonlinear Sciences 等发表学术论文 40 余篇, 主持纵向科研项目十余项；「小范空间计量工作室」公众号创办人。

&emsp;

<div STYLE="page-break-after: always;"></div>

## 3. 杨海生：Part A

### 3.1 课程介绍

**Part A** 将通过为期两天的讲解, 帮助学员们了解并掌握空间相关性的产生缘由、基本的空间计量模型的设定、估计以及相应的实证应用。

具体而言, 我们将回顾各类空间计量模型的由来、发展及演变历程。课程将会详细回顾空间计量的发展历程并介绍其最新的发展。不仅会介绍不同空间模型的设定及其相应的估计方法, 也将讨论使用这些模型的设定思想和结果的经济含义解读。课程的关注重点在于空间计量模型的应用, 我们将利用经典文献的数据集对不同的空间计量模型进行演示, 并提供分析结果所使用的 **Stata+Matlab** 程序, 以便学员们能够这些方法迁移到自己的论文中。此外, 我们还将介绍基本模型的拓展及其在金融、国际贸易、环境经济学、财政以及区域经济学等领域中的应用。最后本课程会结合理论文献的最新进展给学员发放增值福利包。

> 各个专题的具体介绍如下：

**第一讲** 首先回顾三代空间计量模型的设定思想和发展演变历程, 以及不同空间模型之间的差异, 以便各位学员能够合理设定和筛选模型；进而讲解空间计量模型中的三种主要估计方法：MLE、IV 和 GMM 的区别与联系。此外, 我们会引导学员对空间权重矩阵设定进行扩展, 结合最新的两篇理论文献（JoE2020 及 JRSSB21）对投入产出表（连续性空间权重）的设定和 Social Network 的设定进行讲解。

**第二讲** 将在第一讲的基础上引导学员对空间权重矩阵的设定进行扩展, 并讲解如何选择空间模型 （即对 SEM, SAR, SAC, SLX, SDM, SDEM 模型设定的含义及检验进行剖析)。其次, 课程将以 Anselin ([1988](https://www.jianguoyun.com/p/DWWpSZAQtKiFCBj20rwD)) 对犯罪率的研究和 Stata manual 中的数据为例, 讲解如何利用 Stata 和 Matlab 对截面空间模型进行估计和检验。最后, 课程将会对空间模型的估计结果的经济学解释及实证结果的汇报形式进行讲解。

**第三讲** 介绍高阶空间滞后模型及空间联立方程。对于前者, 重点介绍包含高阶空间滞后项的空间计量模型的空间权重设定、IV 及 GMM 估计方法及 Matlab + Stata 实现, 并以杨海生等 (2020, 《经济学 (季刊)》) 为例讲解高阶空间滞后模型的拓展及应用；对于后者, 首先以 Kelejian and Prucha ([2004](https://www.jianguoyun.com/p/DRxqT1IQtKiFCBiMnPcD)) 为例, 讲解如何在 Stata 中设定和估计空间联立方程模型, 进而结合 Chen, et al. ([2015](https://www.jianguoyun.com/p/DQezotAQtKiFCBjwlPcD)) 的文章讲解该模型如何在金融、国际贸易、财政及区域经济学领域进行拓展。

**第四讲** 针对学员们普遍关心 (也是目前空间计量应用中最为常见的数据结构) 的面板数据, 本课程将引导学员使用 Matlab 和 Stata 两种软件对静态空间面板模型进行估计和解读, 辅以多个案例数据剖析静态空间面板应用过程中的主要陷阱。

在课程的最后一部分, 我们将通过一个热门话题 —— **同行效应** 的深入剖析来理解经典空间计量模型在金融领域特别是实证金融中的应用, 以便各位了解从「讲故事」到「建模型」这一过程中可能遇到的各种障碍和解决方法。首先, 通过对 Peer Effect 的刻画, 把空间计量模型的应用扩展到实证金融的诸多领域, 然后讲解 Peer Effect 中公司之间交互影响的微观理论基础, 同时介绍 Peer Effect 识别问题和参数估计的 Stata + Matlab 实现。

作为 Peer Effect 的补充, 课程会结合 li and Schurhoff (2019, JF) 讲解金融网络（Financial Network）的研究的一些技巧。此外, 本课程将结合空间计量模型的前沿研究分享一系列的增值福利, 主要是关于 **时间和空间异质性** 问题以及网络中心度和稳定性的计算与可视化。

<div STYLE="page-break-after: always;"></div>

### 3.2 课程大纲

> #### **第 1 讲** &ensp; 何谓空间计量？ (3 小时)

- 空间计量模型的由来与发展
- 空间权重矩阵的设定与实现
  - ${\color{red}{\text{New!}}}$ 案例：投入产出表设定（Pesaran and Yang, 2020, JoE）；Social Network 设定（Chen et.al, 2021, JRSSB）
- 空间计量模型的估计方法：MLE、IV、GMM 的区别与联系
- 论文解读：
  - Elhorst ([2014](https://www.jianguoyun.com/p/DRlTNMwQtKiFCBiXnPcD))
  - Lee ([2007](https://www.jianguoyun.com/p/Dfn49pgQtKiFCBihnPcD))
  - LeSage and Pace ([2009](https://www.jianguoyun.com/p/DZWmPacQtKiFCBiknPcD))
  - ${\color{red}{\text{New!}}}$ Aquaro, Bailey and Pesaran ([2021](https://www.jianguoyun.com/p/DS-tCgoQtKiFCBiZk_cD))
  - ${\color{red}{\text{New!}}}$ Keane and Neal ([2020](https://www.jianguoyun.com/p/DTBst8oQtKiFCBiuk_cD))

> #### **第 2 讲** &ensp; 截面空间计量模型的扩展 (3 小时)

- 主要的空间计量模型
- 模型的选择与检验
- 模型的展示与解释
- 如何克服空间权重矩阵内生性
- 论文解读：Matlab + Stata 实现
  - Qu and Lee ([2015](https://www.jianguoyun.com/p/DREVkx8QtKiFCBjlk_cD), JoE)
  - Qu, Wang and Lee ([2016](https://www.jianguoyun.com/p/DePTVroQtKiFCBjak_cD), EJ)
- 空间计量模型实操：Anselin ([1988](https://www.jianguoyun.com/p/DWWpSZAQtKiFCBj20rwD)), 犯罪率和家庭收入的截面空间实证模型 (Matlab+Stata)
  –Stata manual 空间工具箱应用

> #### **第 3 讲** &ensp; 高阶空间滞后和空间联立方程模型 (3 小时)

- 高阶空间滞后模型 (1.5 小时)
  - 模型的由来 (Case et al., [1993](https://www.jianguoyun.com/p/De7LdoYQtKiFCBixnPcD), JPE)
  - 模型的估计 (IV 和 GMM) Lee and Liu ([2010](https://www.jianguoyun.com/p/DUxzyb4QtKiFCBi7nPcD), ET)
  - ${\color{red}{\text{New!}}}$ Matlab + Stata 实现及应用：杨海生等 ([2020](https://file.lianxh.cn/Refs/SP/Yang/%E6%9D%A8%E6%B5%B7%E7%94%9F-2020-%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%AD%A3%E5%88%8A.pdf), 《经济学 (季刊)》)
- 空间联立方程模型 (1.5 小时)
  - 空间联立方程的简介：Chen et al. ([2015](https://www.jianguoyun.com/p/DQezotAQtKiFCBjwlPcD), EG)
  - 空间联立方程的估计：Kelejian & Prucha ([2004](https://www.jianguoyun.com/p/DRxqT1IQtKiFCBiMnPcD), JoE)
  - 空间联立方程的 Stata 实现及应用
  - ${\color{red}{\text{New!}}}$ 福利：Pesaran and Yang ([2020](https://www.jianguoyun.com/p/DWDPCvwQtKiFCBi_nPcD)), dominant units 的计算

> #### **第 4 讲** &ensp; 空间面板和同群效应

- 空间面板数据模型简介
- Stata manual 空间工具箱应用
  - 论文解读：Pesaran and Yang ([2020](https://www.jianguoyun.com/p/DWDPCvwQtKiFCBi_nPcD))
- 应用：同群效应 (Peer Effect) 实例剖析
  - Peer Effect 与空间计量：由来与设定
  - 资产定价中的 Peer Effect (Stata+Matlab)：Pirinsky & Wang ([2006](https://www.jianguoyun.com/p/DdMEtDcQtKiFCBjNnPcD), JF)； Parsons et al. ([2020](https://www.jianguoyun.com/p/DV3TbwcQtKiFCBijmPcD), RFS)
  - 公司决策中的 Peer Effect (Stata)：Foucault & Fresard ([2014](https://www.jianguoyun.com/p/DdHci88QtKiFCBjfnPcD), JFE)；Seo et al. ([2021](https://www.jianguoyun.com/p/DV6ijgQQtKiFCBiHmfcD), JAE)
  - Peer Effect 的识别拓展 (工具变量的选择、谁向谁学习或模仿)：Leary and Roberts ([2014](https://www.jianguoyun.com/p/DW8ytzsQtKiFCBjgnPcD), JF)；连玉君等 ([2020](https://www.jianguoyun.com/p/DTxGRUoQtKiFCBilmfcD), 会计研究)
  - ${\color{red}{\text{New!}}}$ 增值福利 1：Blasques et al. ([2016](https://www.jianguoyun.com/p/DV4zHEUQtKiFCBjpnPcD), JoE)；Hauzenberger and Pfarrhofer ([2021](https://www.jianguoyun.com/p/DZJ1EgAQtKiFCBiGnfcD), JoE)
  - ${\color{red}{\text{New!}}}$ 案例应用：Lu Li and Yang ([2021](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/Yang/Lu_Li_Yang-2021_JAE.pdf), JAE)；Lu Li and Yang (2022, EM, R&R, 上课时发送)
  - ${\color{red}{\text{New!}}}$ 增值福利 2：金融网络（Financial Network）分析小技巧-网络中心度及网络稳定性的计算及可视化，Li and Schürhoff ([2019](https://www.jianguoyun.com/p/DXzOQLYQtKiFCBjg0s8EIAA), JF)

### 3.3 参考文献

> **打包下载：** [点击查看所有文献](https://www.jianguoyun.com/p/DfRQsIkQtKiFCBiYm_cD)

- **Anselin**, Luc. **1988**. Spatial Econometrics: Methods and Models. [-Link-](https://www.jianguoyun.com/p/DWWpSZAQtKiFCBj20rwD)
- **Aquaro** M., Bailey N., Pesaran M. H., **2021**, Estimation and Inference for Spatial Models with Heterogeneous Coefficients: An Application to U.S. House Prices[J]. Journal of Applied Econometrics 36 (1): 18–44. [-PDF-](https://www.jianguoyun.com/p/DS-tCgoQtKiFCBiZk_cD)
- **Blasques** F, Koopman S J, Lucas A, et al. **2016**, Spillover dynamics for systemic risk measurement using spatial financial time series models[J]. Journal of Econometrics, 195(2): 211-223. [-PDF-](https://www.jianguoyun.com/p/DV4zHEUQtKiFCBjpnPcD)
- **Case** A. C. , Rosen H. S., Hines J. R., **1993**, Budget spillovers and fiscal policy interdependence: Evidence from the states[J]. Journal of Public Economics, 52 (3): 285-307. [-PDF-](https://www.jianguoyun.com/p/De7LdoYQtKiFCBixnPcD)
- **Chen** Shaoling, Wang Susheng, Yang Haisheng, **2015**, Spatial Competition and Interdependence in Strategic Decisions: Empirical Evidence from Franchising[J]. Economic Geography, 91 (2): 165-204. [-PDF-](https://www.jianguoyun.com/p/DQezotAQtKiFCBjwlPcD)
- **Elhorst** J. P., **2014**, Spatial econometrics: from cross-sectional data to spatial panels [M], Heidelberg, New York, Dordrecht, London: Springer. [-PDF-](https://www.jianguoyun.com/p/DRlTNMwQtKiFCBiXnPcD)
- **Foucault** T., Fresard L., **2014**, Learning from Peers' Stock Prices and Corporate Investment[J]. Journal of Financial Economics, 111 (3): 554-577. [-PDF-](https://www.jianguoyun.com/p/DdHci88QtKiFCBjfnPcD)
- **Hauzenberger**, Niko, and Michael Pfarrhofer. **2021**. Bayesian State‐space Modeling for Analyzing Heterogeneous Network Effects of US Monetary Policy. The Scandinavian Journal of Economics, forthcoming. [-PDF-](https://www.jianguoyun.com/p/DZJ1EgAQtKiFCBiGnfcD)
- **Keane** M., Neal. T, **2020**, Climate Change and U.S. Agriculture: Accounting for Multidimensional Slope Heterogeneity in Panel Data[J], Quantitative Economics, 11 (4): 1391–1429. [-PDF-](https://www.jianguoyun.com/p/DTBst8oQtKiFCBiuk_cD), [-Link-](https://qeconomics.org/ojs/index.php/qe/article/view/1382)
- **Kelejian** H. H., Prucha I. R., **2010**, Specification and estimation of spatial autoregressive models with autoregressive and heteroskedastic disturbances[J]. Journal of Econometrics, 157 (1): 53-67. [-PDF-](https://www.jianguoyun.com/p/DY_VZXEQh8O_CBi7xbwD)
- **Leary** M. T., Roberts M. R., **2014**, Do Peer Firms Affect Corporate Financial Policy?[J]. Journal of Finance, 69 (1): 139–178. [-PDF-](https://www.jianguoyun.com/p/DW8ytzsQtKiFCBjgnPcD)
- **Lee** Lung-fei, Jihai Yu, **2010**, Estimation of spatial autoregressive panel data models with fixed effects[J]. Journal of Econometrics,154 (2): 165-185. [-PDF-](https://www.jianguoyun.com/p/DUxzyb4QtKiFCBi7nPcD)
- **Lee** Lung-Fei, **2007**, GMM and 2SLS estimation of mixed regressive, spatial autoregressive models[J]. Journal of Econometrics, 137 (2): 489-514. [-PDF-](https://www.jianguoyun.com/p/Dfn49pgQtKiFCBihnPcD)
- **Lee** Lung-fei, Xiaodong Liu, **2010**, Efficient GMM estimation of high order spatial autoregressive models with autoregressive disturbances[J]. Econometric Theory, 26 (1): 44. [-PDF-](https://www.jianguoyun.com/p/DUxzyb4QtKiFCBi7nPcD)
- **LeSage** J. P., Pace R. K., **2009**, Introduction to spatial econometrics [M], New York: CRC Press Taylor & Francis Group. [-PDF-](https://www.jianguoyun.com/p/DZWmPacQtKiFCBiknPcD)
- **Li**, D., N. Schürhoff, **2019**, Dealer networks, **Journal of Finance**, 74 (1): 91-144. [-Link-](https://doi.org/10.1111/jofi.12728), [-PDF-](https://www.jianguoyun.com/p/DXzOQLYQtKiFCBjg0s8EIAA)
- **Lu**, Y., J. Li, H. Yang, **2021**, Time-varying inter-urban housing price spillovers in china: Causes and consequences, **Journal of Asian Economics**, 77: 101396. [-Link-](https://doi.org/10.1016/j.asieco.2021.101396), [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/Yang/Lu_Li_Yang-2021_JAE.pdf)
- **Parsons**, C. A., R. Sabbatucci, and S. Titman. **2020**. “Geographic Lead-Lag Effects.” Review of Financial Studies 33 (10): 4721–70. [-PDF-]()
- **Pesaran** M. H., Yang C. F., **2020**, Econometric Analysis of Production Networks with Dominant Units[J]. Journal of Econometrics, 219(2): 507-541. [-PDF-](https://www.jianguoyun.com/p/DWDPCvwQtKiFCBi_nPcD)
- **Pirinsky** C., Wang Q, **2006**, Does Corporate Headquarters Location Matter for Stock Returns?[J]. Journal of Finance, 61 (4): 1991-2015. [-PDF-](https://www.jianguoyun.com/p/DdMEtDcQtKiFCBjNnPcD)
- **Qu**, Xi, and Lung-fei Lee. **2015**. Estimating a Spatial Autoregressive Model with an Endogenous Spatial Weight Matrix. Journal of Econometrics 184 (2): 209–32. [-PDF-](https://www.jianguoyun.com/p/DREVkx8QtKiFCBjlk_cD)
- **Qu**, Xi, Xiaoliang Wang, and Lung‐fei Lee. **2016**. Instrumental Variable Estimation of a Spatial Dynamic Panel Model with Endogenous Spatial Weights When T Is Small. Econometrics Journal 19 (3): 261–90. [-PDF-](https://www.jianguoyun.com/p/DePTVroQtKiFCBjak_cD), [-Link-](https://academic.oup.com/ectj/article-abstract/19/3/261/5056408)
- **Seo**, Hojun. **2021**. Peer Effects in Corporate Disclosure Decisions. Journal of Accounting and Economics 71 (1): 101364. [-PDF-](https://www.jianguoyun.com/p/DV6ijgQQtKiFCBiHmfcD)
- **连玉君**, 彭镇, 蔡菁, 杨海生. 经济周期下资本结构同群效应研究. 会计研究, **2020**, (11): 85-97. [-PDF-](), [-PDF2-](https://www.jianguoyun.com/p/DTxGRUoQtKiFCBilmfcD)
- **杨海生**, 柳建华, 连玉君, 江颖臻, 企业投资决策中的同行效应研究：模仿与学习, 经济学(季刊), **2020**, 19(4): 1375-1400. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/Yang/%E6%9D%A8%E6%B5%B7%E7%94%9F-2020-%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%AD%A3%E5%88%8A.pdf)

&emsp;

<div STYLE="page-break-after: always;"></div>

## 4. 范巧：Part B

### 4.1 课程介绍

**Part B** 将通过为期两天的讲解, 帮助学员了解并掌握动态面板空间计量模型、空间双重差分模型、地理加权回归模型、面向面板数据的时空地理加权回归模型及其 MATLAB 软件实现。具体来说, 我们将回顾空间权重矩阵的设计方式及面板数据空间计量模型的建模范式, 并在此基础上阐释动态和非平稳空间面板模型的建模框架；我们还将纳入空间双重差分模型, 解析纳入空间因素后政策评价模型的演化, 并阐释其平行趋势和安慰剂检验等特定问题；同时, 一些最新的局部分析模型也将被纳入课程, 包括地理加权回归模型、面向面板数据的时空地理加权回归模型等, 以解析解释变量参数的空间漂移性。本课程将嵌入主讲嘉宾近期的理论研究成果, 并提供完整的、简洁的 MATLAB 工具箱及各种模型代码, 方便学员建模使用。此外, 课程所有代码将由主讲嘉宾重新调试和审定。

> 各个专题的具体介绍如下：

**第 5 讲** 首先回顾空间权重矩阵的设计方式, 包括基于空间邻接关系、距离和经济社会规模等的空间权重矩阵、空间权重矩阵的组合与分解、内生时空权重矩阵的设计与遴选；同时, 回顾静态面板数据空间计量模型的建模范式, 包括模型设定、参数估计、模型优选及参数效应分解等；随后介绍动态面板空间计量模型的基本设定, 并结合动态 SDM 面板空间模型、动态 SEM 面板空间模型, 阐释动态面板空间计量模型的建模框架；最后, 还将讨论动态面板空间计量模型的非平稳性及其误差修正模型。

**第 6 讲** 首先阐释空间双重差分模型的基本模型设定, 并解析其参数估计、政策效应分解等细节, 包括全局政策效应分解与局部政策效应分解等；同时, 还将阐释空间双重差分模型的平行趋势检验和安慰剂检验等特定问题。

**第 7 讲** 重点解释地理加权回归模型的建模过程, 包括空间带宽、空间权重矩阵的设定与优选、参数估计、假设检验等；同时, 还将结合主讲嘉宾对地理加权回归模型的近期研究新进展, 阐释地理加权回归模型方法及其新进展。

**第 8 讲** 重点针对 HUANG et al. (2010) 和 FOTHERINGHAM et al.（2015）两篇经典文献关于时空地理加权回归模型的研究, 解析其存在的主要问题；并在基于全息映射的时空权重矩阵设计与优选以及适应面板数据建模需求等基础上, 阐释面向面板数据的时空地理加权回归模型建模框架；同时, 还将结合主讲嘉宾的近期研究新进展, 阐释面向面板数据的时空地理加权回归模型的应用问题。

### 4.2 课程大纲

> #### **第 5 讲** &ensp; 动态与非平稳空间面板模型（3 小时）

- ${\color{red}{\text{New!}}}$ 空间权重矩阵的设计、组合与分解
- ${\color{red}{\text{New!}}}$ 时空权重矩阵的设计与遴选
- 静态空间面板模型的建模范式
- 动态空间面板模型的基本设定
- 动态面板 SDM 模型：模型设定、DGP 过程、参数估计与效应分解
- ${\color{red}{\text{New!}}}$ 非平稳动态面板 SDM 模型及其误差修正模型
- 论文解读：
  - Ertur and Koch, [2007](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/Ertur-2017-Growth-technological-interdependence.pdf), JAE
  - Elhorst et al., [2010](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/Elhorst-2010-Growth-and-convergence.pdf), Geographically Analysis
  - Elhorst, [2005](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/Elhorst-2005-Unconditional-Maximum-Likelihood.pdf), Geographically Analysis
  - Yang et al., [2006](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/Yang-2006-Functional-form.pdf), EL
  - ${\color{red}{\text{New!}}}$ 范巧 and Hudson, [2018](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-Darren-2018-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%8C%85%E5%90%AB%E5%8F%AF%E5%8F%98%E6%97%B6%E9%97%B4%E6%95%88%E5%BA%94%E7%9A%84%E5%86%85%E7%94%9F%E6%97%B6%E7%A9%BA%E6%9D%83%E9%87%8D%E7%9F%A9%E9%98%B5.pdf), 数量经济技术经济研究

> #### **第 6 讲** &ensp; 双重差分空间计量模型（SDID）（3 小时）

- SDID 模型的源起及主要分析范式
- SDID 模型的设定、估计与政策效应分解
- 非线性 SDID 模型的设定、估计与政策效应分解
- SDID 模型的平行趋势检验与安慰剂检验
- 论文解读：
  - Chagas et al., [2016](https://file.lianxh.cn/Refs/SP/FanQ/Chagas-2016-A-spatial-difference-in-differences.pdf)
  - Kolak and Anselin, [2020](https://file.lianxh.cn/Refs/SP/FanQ/Kolak-2020-A-Spatial-Perspective.pdf)

> #### **第 7 讲** &ensp; 地理加权回归模型（3 小时）

- GWR 模型的建模范式
- GWR 模型中空间权重矩阵的设计与延展
- GWR 模型建模中特定问题处理
- ${\color{red}{\text{New!}}}$ 多尺度地理加权回归模型（MGWR）的建模范式
- ${\color{red}{\text{New!}}}$ 地理加权回归模型方法的新进展
- 论文解读：
  - Harris et al., [2011](https://file.lianxh.cn/Refs/SP/FanQ/Harris-2011-Geographically-weighted-PCA.pdf)
  - Oshan and Fotheringham, [2018](https://file.lianxh.cn/Refs/SP/FanQ/Oshan-2018-A-Comparison-of-Spatially-Varying-Regression.pdf)
  - Fotheringham et al., [2017](https://file.lianxh.cn/Refs/SP/FanQ/Fotheringham-2017-Multiscale.pdf)
  - Chen and Mei, [2020](https://file.lianxh.cn/Refs/SP/FanQ/Chen-2020-Scale-adaptive-estimation.pdf)
  - ${\color{red}{\text{New!}}}$ 范巧和郭爱君, [2021a](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B%E6%96%B9%E6%B3%95%E4%B8%8E%E7%A0%94%E7%A9%B6%E6%96%B0%E8%BF%9B%E5%B1%95.pdf), [2021b](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%9F%BA%E4%BA%8E%E5%85%A8%E6%81%AF%E6%98%A0%E5%B0%84%E7%9A%84%E9%9D%A2%E6%9D%BF%E6%97%B6%E7%A9%BA%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B.pdf)

> #### **第 8 讲** &ensp; 面板时空地理加权回归模型(PGTWR)（3 小时）

- 经典时空地理加权回归模型(GTWR)及其存在的问题
- PGTWR 模型的基本设定
- PGTWR 模型时空权重矩阵的设计与优选
- PGTWR 模型参数估计及假设检验
- ${\color{red}{\text{New!}}}$ PGTWR 模型的应用
- 论文解读：
  - Huang et al., [2010](https://file.lianxh.cn/Refs/SP/FanQ/Huang-2010-Geographically-and-temporally.pdf), JGIS
  - Fotheringham et al., [2015](https://file.lianxh.cn/Refs/SP/FanQ/Fotheringham-2015-Geographical-and-Temporal.pdf), Geographical Analysis
  - Du et al., [2018](https://file.lianxh.cn/Refs/SP/FanQ/Du-2018-Extending-geographically.pdf), Ecological Informatics
  - Bidanset et al., [2018](https://file.lianxh.cn/Refs/SP/FanQ/Bidanset-2018-Accounting-for-Locational.pdf), JPTAA
  - ${\color{red}{\text{New!}}}$ 范巧和郭爱君, [2021](https://file.lianxh.cn/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%9F%BA%E4%BA%8E%E5%85%A8%E6%81%AF%E6%98%A0%E5%B0%84%E7%9A%84%E9%9D%A2%E6%9D%BF%E6%97%B6%E7%A9%BA%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B.pdf), 数量经济技术经济研究
  - ${\color{red}{\text{New!}}}$ 郭爱君和范巧, [2022](https://file.lianxh.cn/Refs/SP/FanQ/%E9%83%AD%E7%88%B1%E5%90%9B-%E8%8C%83%E5%B7%A7-2022-%E4%B8%AD%E5%9B%BD%E5%9C%B0%E7%BA%A7%E5%B8%82%E5%B7%A5%E4%B8%9A%E5%85%A8%E8%A6%81%E7%B4%A0%E7%94%9F%E4%BA%A7%E7%8E%87%E7%9A%84%E5%B1%80%E9%83%A8%E6%B5%8B%E5%BA%A6%E7%A0%94%E7%A9%B6.pdf), 数量经济技术经济研究

<div STYLE="page-break-after: always;"></div>

### 4.3 参考文献

**温馨提示：** 文中链接在微信中无法生效。请点击底部<font color=DarkGreen>「阅读原文」</font>。

> **打包下载：** [点击查看所有文献](https://www.jianguoyun.com/p/De7Bo8MQtKiFCBia0c8EIAA)

- **Bidanset** P., et al., **2018**, Accounting for locational, temporal, and physical similarity of residential sales in mass appraisal modeling: introducing the development and application of geographically, temporally, and characteristically weighted regression, Journal of Property Tax Assessment & Administration, 14(02): 4~12. [-PDF-](https://www.jianguoyun.com/p/DfgTQLQQtKiFCBjSsLwD)
- **Chagas** A. L. S., Azzoni C. R., Almeida A. N., **2016**, A spatial difference-in-differences analysis of the impact of sugarcane production on respiratory diseases, Regional Science and Urban Economics, 59: 24~36. [-PDF-](https://www.jianguoyun.com/p/DYyjzgQQtKiFCBjVtLwD)
- **Chen** F., Mei C., **2020**, Scale-adaptive estimation of mixed geographically weighted regression models, 12.[J], Economic Modelling, doi.org/10.1016/j.econmod.2020.02.015. [-PDF-](https://www.jianguoyun.com/p/DYCIFVMQtKiFCBjXtLwD)
- **Du** Z., et al., **2018**, Extending geographically and temporally weighted regression to account for both spatiotemporal heterogeneity and seasonal variations in coastal seas, Ecological Informatics, 43: 185~199. [-PDF-](https://www.jianguoyun.com/p/Dd6sJ0YQtKiFCBjYtLwD)
- **Elhorst** J. P., **2014**, Spatial econometrics: from cross-sectional data to spatial panels [M], Heidelberg, New York, Dordrecht, London: Springer. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/Elhorst-2014-Spatial-Econometrics.pdf), [-PDF2-](https://www.jianguoyun.com/p/DbVq8w8QtKiFCBjetLwD)
- **Elhorst** J. P., Piras G., Arbia G., **2010**, Growth and convergence in a multiregional model with space–time dynamics, Geographically Analysis, 52(03): 338~355. [-PDF-](https://www.jianguoyun.com/p/DWiCRjMQtKiFCBjZtLwD)
- **Elhorst** J. P., **2005**, Unconditional maximum likelihood estimation of linear and log-linear dynamic models for spatial panels. Geographically Analysis, 37(1): 62~83. [-PDF-](https://www.jianguoyun.com/p/DVtmy4sQtKiFCBjltLwD)
- **Ertur** C., Koch W., **2007**, Growth, technological interdependence and spatial externalities: theory and evidence. Journal of Applied Econometrics, 22(6): 1033~1062. [-PDF-](https://www.jianguoyun.com/p/DX-fcC4QtKiFCBjmtLwD)
- **Fotheringham** A. S., Crespo R., Yao J., **2015**, Geographical and temporal weighted regression (GTWR), Geographical Analysis, 47: 431~452. [-PDF-](https://www.jianguoyun.com/p/DdF_5IQQtKiFCBjptLwD)
- **Fotheringham** A. S., Yang W., Kang W., **2017**, Multiscale Geographically Weighted Regression (MGWR), Annals of American Association of Geographers, 107(06): 1247~1265. [-PDF-](https://www.jianguoyun.com/p/Dan_VagQtKiFCBjxtLwD)
- **Harris** P., Brunsdon C., Charlton M., **2011**, Geographically weighted principal components analysis, International Journal of Geographical Information Science, 25(10): 1717~1736. [-PDF-](https://www.jianguoyun.com/p/DW6iPl0QtKiFCBjztLwD)
- **Huang** B., Wu B., Barry M., **2010**, Geographically and temporally weighted regression for modeling spatio-temporal variation in house prices, International Journal of Geographical Information Science, 24(03): 383~401. [-PDF-](https://www.jianguoyun.com/p/DUfOEtwQtKiFCBj0tLwD)
- **Kolak** M., Anselin L., **2020**, A spatial perspective on the econometrics of program evaluation, International Regional Science Review, 43(1/2): 128~153. [-PDF-](https://www.jianguoyun.com/p/DVUPqx8QtKiFCBj1tLwD)
- **LeSage** J. P., Pace R. K., **2009**, Introduction to spatial econometrics [M], New York: CRC Press Taylor & Francis Group. [-Link-](https://www.jianguoyun.com/p/DcvKR_wQtKiFCBjUtLwD)
- **Oshan** T, M., Fotheringham A. S., **2018**, A comparison of spatially varying regression coefficient estimates using geographically weighted and spatial-filter-based techniques, Geographical Analysis, 50: 53~75. [-PDF-](https://www.jianguoyun.com/p/Da03NOwQtKiFCBj4tLwD)
- **Yang** Z., Li C., Tse Y. K., **2006**, Functional form and spatial dependence in spatial panels, Economics Letters, 91(1): 138~145. [-PDF-](https://www.jianguoyun.com/p/DWWz82gQtKiFCBj5tLwD)
- **范巧**, Hudson Darren. 一种新的包含可变时间效应的内生时空权重矩阵构建方法[J]. 数量经济技术经济研究, 2018, 35(1): 131-149. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-Darren-2018-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%8C%85%E5%90%AB%E5%8F%AF%E5%8F%98%E6%97%B6%E9%97%B4%E6%95%88%E5%BA%94%E7%9A%84%E5%86%85%E7%94%9F%E6%97%B6%E7%A9%BA%E6%9D%83%E9%87%8D%E7%9F%A9%E9%98%B5.pdf)
- **范巧**, 郭爱君. 地理加权回归模型方法与研究新进展[J]. 数量经济研究,2021, 12(02): 134-150. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B%E6%96%B9%E6%B3%95%E4%B8%8E%E7%A0%94%E7%A9%B6%E6%96%B0%E8%BF%9B%E5%B1%95.pdf)
- **范巧**, 郭爱君. 2021. 一种新的基于全息映射的面板时空地理加权回归模型方法[J]. 数量经济技术经济研究, 38 (4): 120-138. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/%E8%8C%83%E5%B7%A7-2021-%E4%B8%80%E7%A7%8D%E6%96%B0%E7%9A%84%E5%9F%BA%E4%BA%8E%E5%85%A8%E6%81%AF%E6%98%A0%E5%B0%84%E7%9A%84%E9%9D%A2%E6%9D%BF%E6%97%B6%E7%A9%BA%E5%9C%B0%E7%90%86%E5%8A%A0%E6%9D%83%E5%9B%9E%E5%BD%92%E6%A8%A1%E5%9E%8B.pdf), [-PDF2-](https://www.jianguoyun.com/p/DWUYJa4QtKiFCBjWkvcD)
- **郭爱君**, 范巧. 中国地级市工业全要素生产率的局部测度研究[J]. 数量经济技术经济研究, 2022, 39(06): 61-80. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/SP/FanQ/%E9%83%AD%E7%88%B1%E5%90%9B-%E8%8C%83%E5%B7%A7-2022-%E4%B8%AD%E5%9B%BD%E5%9C%B0%E7%BA%A7%E5%B8%82%E5%B7%A5%E4%B8%9A%E5%85%A8%E8%A6%81%E7%B4%A0%E7%94%9F%E4%BA%A7%E7%8E%87%E7%9A%84%E5%B1%80%E9%83%A8%E6%B5%8B%E5%BA%A6%E7%A0%94%E7%A9%B6.pdf)

&emsp;

<div STYLE="page-break-after: always;"></div>

## 5. 报名信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**：3800 元/人 (全价)
- **优惠方案**：
  - **老学员 (现场班/专题课)/团报（3 人及以上）：** 9 折, 3420 元/人
  - **学生**（需提供学生证照片）：9 折, 3420 元/人
  - **会员：** 85 折, 3230 元/人
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：18636102467 (微信同号）

&emsp;

> **报名链接：** [http://junquan18903405450.mikecrm.com/EmGijiK](http://junquan18903405450.mikecrm.com/EmGijiK)

> :key: 长按/扫描二维码报名：    
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：空间计量.png)

### 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时, 请务必提供「**汇款人姓名-单位**」信息, 以便确认。

> **方式 2：扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/君泉收款码.png)

> **温馨提示：** 扫码支付后, 请将「**付款记录**」截屏发给王老师-18903405450（微信同号）

&emsp;

## 6. 听课指南

### 6.1 软件

- **听课软件**：本次课程可以在手机, ipad , 平板以及 windows/Mac 系统的电脑上听课（win 台式机除外）。

> #### **特别提示：**

- 为保护讲师的知识产权和您的账户安全, 系统会自动在您观看的视频中嵌入您的「用户名」信息
- 一个账号绑定一个设备, 且听课电脑不能外接显示屏, 请大家提前准备好自己的听课设备。
- 本课程为虚拟产品, **一经报名, 不得退换**。

### 6.2 实名制报名

本次课程实行实名参与, 具体要求如下：

- 高校老师/同学报名时需要向连享会课程负责人 **提供真实姓名, 并附教师证/学生证图片**；
- 研究所及其他单位报名需提供 **能够证明姓名以及工作单位的证明**；
- 报名即默认同意「[<font color=darkred>**连享会版权保护协议条款**</font>](https://www.lianxh.cn/news/b16b512ee620b.html)」。

&emsp;

<div STYLE="page-break-after: always;"></div>

## 7. 助教招聘

> ### 说明和要求

- **名额：** 10 名
- **任务：**
  - **A. 课前准备**：协助完成 3 篇介绍 Stata /MATLAB 和计量经济学基础知识的文档；
  - **B. 开课前答疑**：协助学员安装课件和软件, 在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容, 在微信群中答疑 (8:00-9:00, 19:00-22:00)；
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职, 熟悉 Stata 的基本语法和常用命令, 能对常见问题进行解答和记录 (往期助教优先录用)。
- **截止时间：** 2022 年 9 月 1 日 (将于 9 月 4 日公布遴选结果于 [课程主页](https://gitee.com/lianxh/SP), 及 连享会主页 [lianxh.cn](https://www.lianxh.cn))

> **申请链接：** <https://www.wjx.top/jq/90823024.aspx>

> 扫码填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教招聘：空间计量.png)

> :apple: **课程主页：** <https://gitee.com/lianxh/SP>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

<div STYLE="page-break-after: always;"></div>

> ## 关于我们

- **Stata 连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，900+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。

&emsp;

> ## 相关课程

> ### 免费公开课

- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1 小时 40 分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[Bilibili 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[Bilibili 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin)
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles 等)

> #### :cat: [课程列表](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

> <font color=red>New！</font> **`lianxh` 命令发布了：**  
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：  
> &emsp; `. help lianxh`

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)
